# Changelog 

## [Unreleased]

## [2.0.0] 29-10-2020

## Changed
- Estructura del changelog 

## [1.1.1] 29-10-2020
### Added
- Se ha creado una lista de esferas y disparos
- Las esferas disminuyen su tamaño con el paso del tiempo
- Se crean y se destruyen las esferas y los disparos correctamente
- Se limita el número de disparos a 3 

## [1.0]
