// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "Mundo.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{

}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);

	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++)
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );

	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0)
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();
	////////////
	//Dibujamos los vectores de disparos y esferas:
	for(int i=0; i<esferas.size(); i++){esferas[i].Dibuja();}
	for(int i=0; i<disparos1.size(); i++){disparos1[i].Dibuja();}
	for(int i=0; i<disparos2.size(); i++){disparos2[i].Dibuja();}
	/////////////////////////////////////
	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();

	//esfera.Dibuja();//de momento se deja pero luego se borra

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{
	//incrememtamos el valor de temp por cada llamada:
	temp++;
	if(temp==800){//este numero aqui a fuego hay que cambiarlo
		//se llama a la funcion reducirEsfera:
		reducirEsfera();
		//se llama funcion agreacion de esferas:
		agregarEsfera();
		temp=0; //reiniciamos el temporizador
	}
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	//esfera.Mueve(0.025f);
	//hacemos que se muevan las pelotas del vector:
	for(int i=0; i<esferas.size(); i++){esferas[i].Mueve(0.025f);}
	//hacemos que se muevan los disparos:
	//comprobamos si han chocado:
	for(int i=0; i<disparos1.size(); i++){disparos1[i].Mueve(0.025f);}
	for(int i=0; i<disparos2.size(); i++){disparos2[i].Mueve(0.025f);}

	int i;
	for(i=0;i<paredes.size();i++)
	{
		//paredes[i].Rebota(esfera);//esto va a sobrar
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
		//cada pared comprueba si ha rebotado con cada pelota:
		for (int j=0; j<esferas.size(); j++) {paredes[i].Rebota(esferas[j]);}
	}

	//jugador1.Rebota(esfera);//esto va a sobrar
	//jugador2.Rebota(esfera); //esto va a sobrar

	//cada jugador comprueba que ha rebotado con cada esfera:
	for(int i=0; i<esferas.size(); i++){
		jugador1.Rebota(esferas[i]);
		jugador2.Rebota(esferas[i]);
	}
	//comprobamos que no ha habido punto por cada esfera:
	for(int i=0;i<esferas.size();i++)
	{
		//de momento vuelven todas las pelotas a aparecer
		//despues se hara que se eliminen todas las pelotas -1:
		if(fondo_izq.Rebota(esferas[i]))
		{
			//si solo queda una no la borramos
			if(esferas.size()==1)
			{
				esferas[i].centro.x=0;
				esferas[i].centro.y=rand()/(float)RAND_MAX;
				esferas[i].velocidad.x=2+2*rand()/(float)RAND_MAX;
				esferas[i].velocidad.y=2+2*rand()/(float)RAND_MAX;
			}
			//si hay mas una vez que hace punto se borra:
			else{
				esferas.erase(esferas.begin()+i); //eliminamos la esfera
			}
			puntos2++;
		}
		if(fondo_dcho.Rebota(esferas[i]))
		{
			//si hay una no se borra
			if(esferas.size()==1){
				esferas[i].centro.x=0;
				esferas[i].centro.y=rand()/(float)RAND_MAX;
				esferas[i].velocidad.x=-2-2*rand()/(float)RAND_MAX;
				esferas[i].velocidad.y=-2-2*rand()/(float)RAND_MAX;
			}
			//si hay mas se borra
			else{
				esferas.erase(esferas.begin()+i);
			}
			puntos1++;
		}
	}
	//comprobamos si los disparos chocan con la pared del fondo derecho:
	for(int i=0; i<disparos1.size(); i++){
		if(fondo_dcho.Rebota(disparos1[i])){
			disparos1.erase(disparos1.begin()+i);
		}
	}
	//comprobamos si los disparos chocan con la pared del fondo izquierdo:
	for(int i=0; i<disparos2.size(); i++){
		if(fondo_izq.Rebota(disparos2[i])){
			disparos2.erase(disparos2.begin()+i);
		}
	}
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	//disparos
	case 'd':agregarDisparo1();break;
	case 'k':agregarDisparo2();break;

	}
}

void CMundo::Init()
{
	//inicializamos temp:
	temp=0;
	//iniciamos con una esfera en el vector:
	//debemos poner aqui funcion agregarEsfera
	agregarEsfera();
	//Esfera e;
	//esferas.push_back(e);
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}

void CMundo::agregarEsfera(){
	Esfera e;
	esferas.push_back(e);
}

void CMundo::reducirEsfera(){
	//recorremos todo el vector:
	for(int i=0; i<esferas.size(); i++){
		if(esferas[i].radio>=0.1f){//este numero aqui a fuego hay que cambiarlo
			esferas[i].radio=esferas[i].radio*0.8; //90% de su tamaño
		}
	}
}

void CMundo::agregarDisparo1(){
	Esfera d;
	//le damos un radio, la posicion del jugador y velocidad x:
	d.radio=0.1f;
	d.centro.x=(jugador1.x1+jugador1.x2)/2;
	d.centro.y=(jugador1.y1+jugador1.y2)/2;
	d.velocidad.y=0;
	d.velocidad.x=5;
	//ahora agregamos a la lista de disparos:
	if(disparos1.size()<3){disparos1.push_back(d);} //no mas de 3 disparos
}

void CMundo::agregarDisparo2(){
	Esfera d;
	//le damos un radio, la posicion del jugador y velocidad x:
	d.radio=0.1f;
	d.centro.x=(jugador2.x1+jugador2.x2)/2;
	d.centro.y=(jugador2.y1+jugador2.y2)/2;
	d.velocidad.y=0;
	d.velocidad.x=-5;
	//ahora agregamos a la lista de disparos:
	if(disparos2.size()<3){disparos2.push_back(d);}
}
