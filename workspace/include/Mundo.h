//AUTOR: ALEXANDER AMANI 53329
// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"

class CMundo
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();

	void InitGL();
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();
	//funcion que añade pelotas
	void agregarEsfera();
	//funcion que reduce tamaño de pelotas:
	void reducirEsfera();
	//funcion agregar disaparos:
	void agregarDisparo1();
	void agregarDisparo2();
	//Esfera esfera; esto hay que borrar
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	//////////////////
	std::vector<Esfera> esferas;
	std::vector<Esfera> disparos1;
	std::vector<Esfera> disparos2;
	//////////////////
	int temp; //variable para contar intervalos de tiempo

	int puntos1;
	int puntos2;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
